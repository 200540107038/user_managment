import 'package:app_guru/api/api_page.dart';
import 'package:app_guru/crud_database/liked_user.dart';
import 'package:app_guru/crud_database/user_list.dart';
import 'package:app_guru/ui_screen.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final tab = [
    const UiScreen(),
    const UserList(),
    const AllName(),
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: tab[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          iconSize: 32,
          backgroundColor: Colors.deepPurple,
          selectedItemColor: Colors.black87,
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(icon: Icon(Icons.add), label: "CRUD"),
            BottomNavigationBarItem(icon: Icon(Icons.api), label: "API"),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
      ),
    );
  }
}
