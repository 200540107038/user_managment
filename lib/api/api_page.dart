import 'dart:convert';
import 'package:app_guru/home_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'add_data.dart';


class AllName extends StatefulWidget {
  const AllName({super.key});

  @override
  State<AllName> createState() => _AllNameState();
}

class _AllNameState extends State<AllName> {
  bool isLoading = true;
  List Name = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchNamesData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Name List"),
        leading: IconButton(
          onPressed: () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          },
          icon: (Icon(Icons.arrow_back)),
        ),
      ),
      body: Visibility(
        visible: isLoading,
        child: Center(
          child: CircularProgressIndicator(),
        ),
        replacement: Visibility(
          visible: Name.isEmpty,
          child: Center(
            child: Text("No Names"),
          ),
          replacement: ListView.builder(
            itemCount: Name.length,
            itemBuilder: (context, index) {
              final name = Name[index] as Map;
              final nameid = name["id"] as String;
              return Card(
                child: ListTile(
                  leading: CircleAvatar(
                    child: Text("${index + 1}"),
                  ),
                  title: Text(name["name"]),
                  trailing: PopupMenuButton(
                    onSelected: (value) {
                      if (value == "edit") {
                        print(value);
                        navigateToEditNamePage(name);
                      } else if (value == "delete") {
                        deleteById(nameid);
                      }
                    },
                    itemBuilder: (context) {
                      return [
                        PopupMenuItem(
                          child: Text("Delete"),
                          value: "delete",
                        ),
                        PopupMenuItem(
                          child: Text("Edit"),
                          value: "edit",
                        ),
                      ];
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: navigateToAddNamePage,
        label: Text("Add Name"),
      ),
    );
  }

  Future<void> navigateToAddNamePage() async {
    final route = MaterialPageRoute(
      builder: (context) => AddNamePage(),
    );
    await Navigator.push(context, route);
    setState(() {
      isLoading = true;
    });
    fetchNamesData();
  }

  Future<void> navigateToEditNamePage(Map book) async {
    final route = MaterialPageRoute(
      builder: (context) => AddNamePage(
        name: book,
      ),
    );
    await Navigator.push(context, route);
    setState(() {
      isLoading = true;
    });
    fetchNamesData();
  }

  Future<void> fetchNamesData() async {
    final url = "https://63972eaa86d04c76338d939a.mockapi.io/student_details";
    final uri = Uri.parse(url);

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List;
      if (json != null) {
        setState(() {
          Name = json;
        });
      } else {
        print("Data is Null");
      }
    } else {
      print("Something Went Wrong");
    }
    setState(() {
      isLoading = false;
    });
  }

  Future<void> deleteById(String id) async {
    final url = "https://63972eaa86d04c76338d939a.mockapi.io/student_details/$id";
    final uri = Uri.parse(url);
    final response = await http.delete(uri);

    if (response.statusCode == 200) {
      final filtered = Name.where((element) => element["id"] != id).toList();
      setState(() {
        Name = filtered;
      });
    } else {
      print("Deletion Failed");
    }
  }
}