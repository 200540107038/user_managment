import 'package:app_guru/crud_database/database/dbhelper.dart';
import 'package:app_guru/crud_database/edit.dart';
import 'package:app_guru/crud_database/liked_user.dart';
import 'package:app_guru/crud_database/model/detail.dart';
import 'package:app_guru/crud_database/tap_user.dart';
import 'package:app_guru/home_page.dart';
import 'package:app_guru/crud_database/add_user.dart';
import 'package:flutter/material.dart';

class UserList extends StatefulWidget {
  const UserList({Key? key}) : super(key: key);
  @override
  State<UserList> createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  final dbHelper = DatabaseProvider.db;
  List<User> userList = [];
  String filter = '';

  @override
  void initState() {
    super.initState();
    _getUsers();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User List Page'),
        leading: IconButton(
          icon: const Icon(Icons.navigate_before),
          onPressed: (){Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>const HomePage()));
          },
        ),
        actions: [
          IconButton(onPressed: (){
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  LikedUsers(userList: userList)));
          }, icon: const Icon(Icons.favorite_border_outlined))
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: TextField(
              onChanged: (val){
                setState(() {
                  filter = val.toLowerCase();
                });
              },
              decoration:InputDecoration(
                hintText: "Search User Hear....",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: userList.length,
              itemBuilder: (BuildContext context, int index){
                final user = userList[index];
                if(!user.name.toLowerCase().contains(filter)){
                  return Container();
                }
                return ListTile(
                  title: Text(user.name),
                  subtitle: Text(user.city),
                  trailing:
                  Wrap(
                    children: [
                      IconButton(
                        icon: user.isliked
                            ? Icon(Icons.favorite)
                            : Icon(Icons.favorite_border),
                        color: Colors.redAccent,
                        onPressed: (){
                          setState(() {
                            user.isliked = !user.isliked;
                            dbHelper.update(user);
                          });
                        },
                      ),
                      // IconButton(
                      //   onPressed: (){
                      //     setState(() {
                      //       user.isliked = !user.isliked;
                      //       dbHelper.likedUpdate(user);
                      //     });},
                      //   icon: Icon(user.isliked ? Icons.favorite : Icons.favorite_border),),
                      IconButton(
                        icon: const Icon(Icons.edit),
                        onPressed: (){Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => EditUser(user: user,)));},
                      ),
                      IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          _deleteUser(user.id!);
                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const UserList()));
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text('User Deleted Successfully!!'),
                            ),
                          );
                        },
                      ),
                      // IconButton(
                      //   icon: _isFavorited
                      //       ? Icon(Icons.favorite)
                      //       : Icon(Icons.favorite_border_outlined),
                      //   color: Colors.redAccent,
                      //   onPressed: (){
                      //     setState(() {
                      //       _isFavorited =!_isFavorited;
                      //       dbHelper.update(user);
                      //     });
                      //   },
                      // ),
                    ],
                  ),
                  onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => ShowUser(user: user,)));},
                );
              },
            ),
          ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => const AddUser(),
            ),
          ).then((value) => _getUsers());
        },
      ),
    );
  }

  Future<void> _getUsers() async {
    final list = await dbHelper.getAllUsers();
    setState(() {
      userList.clear();
      userList.addAll(list);
    });
  }

  Future<void> _deleteUser(int id) async {
    final count = await dbHelper.delete(id);
    if (count > 0) {
      _deleteUser(id);
    }
  }
}
