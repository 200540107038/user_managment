import 'package:flutter/material.dart';

class UiScreen extends StatelessWidget {
  const UiScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 7,
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fitHeight,
                  image: AssetImage('assets/images/couple.png'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 35,
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                RichText(
                  text: const TextSpan(
                    children: [
                      TextSpan(
                          text: "Welcome To Our App",
                          style: TextStyle(
                              fontSize: 35, color: Colors.deepPurple)),
                    ],
                  ),
                ),
                const Expanded(
                    child: Icon(Icons.favorite,
                        color: Colors.deepPurple, size: 45)),
              ],
            ),
          )
        ],
      ),
    );
  }
}
