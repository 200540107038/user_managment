import 'package:app_guru/home_page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    String name = "";
    var _formKey = GlobalKey<FormState>();

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Image.asset(
                  'assets/images/wedd.png',
                  fit: BoxFit.cover,
                ),
                const SizedBox(
                  height: 35,
                ),
                Text(
                  "Welcome ${name}",
                  style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 16),
                  child: Column(
                    children: [
                      TextFormField(
                        // onChanged: (value) {
                        //   setState(() {
                        //     name = value;
                        //   });
                        // },
                        decoration: const InputDecoration(
                          hintText: 'Enter User Name',
                          labelText: 'User_Name',
                        ),
                        validator: (value){
                          if(value == null || value.isEmpty){
                            return "User Can't be Empty";
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                          hintText: 'Enter Password',
                          labelText: 'Password',
                        ),
                        validator: (value){
                          if(value!.isEmpty){
                            return "Please Enter Password";
                          }else if(value == null || value.length < 6){
                            return "PSW Must be atleast 6 ";
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 55,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          if(_formKey.currentState!.validate()){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const HomePage(),
                              ),
                            );
                          }
                        },
                        style: TextButton.styleFrom(minimumSize: const Size(150, 50)),
                        child: const Text(
                          'Log-in',
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
